package modules

// 格式化字符串数据结构
type ParseString struct {
	Name      string // 注册变量名
	SourceLen int    // 字符串初始长度信息
	NowLen    int    // 字符串当前长度信息
	NowStatus bool   // 字符串修改状态
	Source    string // 字符串原始数据
	Now       string // 字符串当前数据

	Data string // 当前字符串
}
