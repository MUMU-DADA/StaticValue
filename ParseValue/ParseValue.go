package parsevalue

import (
	"fmt"
	"strconv"
	"strings"

	"gitee.com/MUMU-DADA/StaticValue/modules"
)

const (
	MOD_NAME               = "STATIC_VALUE_"                  // 变量名
	MOD_STRING_HEAD        = "STRING_HEAD_"                   // 编码字符头
	MOD_STRING_HEAD_RANDOM = "VSXqtaRZLdqDyK94zuuWpSaFVREugy" // 字符串头随机部分 防止值重复

	RANDOM_NAME_HEAD   = "random_NAME_HEAD_"        // 随机变量名称的头信息
	MAX_VALUE_NAME_LEN = len(RANDOM_NAME_HEAD) + 36 // 变量名称[]byte最大长度

	MAX_STRING_LEN = 1 << 15 // 输入字符编码后[]byte最大长度
)

var (
	NAME_LEN = len(MOD_NAME) + len(MOD_STRING_HEAD) + len(MOD_STRING_HEAD_RANDOM)   // 特征头部字符串[]byte长度
	INFO_LEN = MAX_VALUE_NAME_LEN + len([]byte(strconv.Itoa(MAX_STRING_LEN)))*2 + 1 // 属性信息字符串[]byte长度
	DATA_LEN = MAX_STRING_LEN * 2                                                   // 内容字符串[]byte长度
	ALL_LEN  = NAME_LEN + INFO_LEN + DATA_LEN                                       // 总字符串[]byte长度
)

//
// 字符串编码组成:
//
// 		字符串特征头部 + 字符串属性信息 + 字符串内容
//
// 		字符串特征头部为: MOD_NAME + MOD_STRING_HEAD + MOD_STRING_HEAD_RANDOM 拼接
// 		字符串属性信息为: 字符串名称 + 字符串初始值长度 + 字符串编码后当前真实长度 + 当前变量为初始化还是修改 0为未修改 1为已经修改
// 		字符串内容组成为: 字符串初始值编码 + "0"字符占位 + 一倍最大字符串长度(用于存储变化后的值)
//

// 将目标字符串格式化到正确长度
func parse(data string, maxLen int) string {
	temp := strings.Repeat("0", maxLen-len(data))
	return temp + data
}

// 解码字符串的信息 格式化字符串数据结构
func Decode(v string) (*modules.ParseString, error) {
	ans := &modules.ParseString{
		Data: v,
	}
	if len(v) != ALL_LEN {
		return nil, fmt.Errorf("数据格式错误")
	}
	if v[:NAME_LEN] != MOD_NAME+MOD_STRING_HEAD+MOD_STRING_HEAD_RANDOM {
		return nil, fmt.Errorf("数据头部格式错误")
	}
	// 解析变量名
	ansName := v[NAME_LEN : NAME_LEN+MAX_VALUE_NAME_LEN]
	ans.Name = ansName
	// 解析字符串初始长度信息
	ansDefaultLen, err := strconv.Atoi(v[NAME_LEN+MAX_VALUE_NAME_LEN : NAME_LEN+MAX_VALUE_NAME_LEN+len([]byte(strconv.Itoa(MAX_STRING_LEN)))])
	if err != nil {
		return nil, fmt.Errorf("数据初始长度格式错误")
	}
	ans.SourceLen = ansDefaultLen
	// 解析字符串当前长度信息
	ansNowLen, err := strconv.Atoi(v[NAME_LEN+MAX_VALUE_NAME_LEN+len([]byte(strconv.Itoa(MAX_STRING_LEN))) : NAME_LEN+MAX_VALUE_NAME_LEN+len([]byte(strconv.Itoa(MAX_STRING_LEN)))+len([]byte(strconv.Itoa(MAX_STRING_LEN)))])
	if err != nil {
		return nil, fmt.Errorf("数据当前长度格式错误")
	}
	ans.NowLen = ansNowLen
	// 解析字符串修改状态
	ansNowStatus, err := strconv.ParseBool(string(v[NAME_LEN+INFO_LEN-1]))
	if err != nil {
		return nil, fmt.Errorf("数据当前状态格式错误")
	}
	ans.NowStatus = ansNowStatus
	// 解析字符串原始数据
	ansDefaultData := v[NAME_LEN+INFO_LEN : NAME_LEN+INFO_LEN+MAX_STRING_LEN][MAX_STRING_LEN-ansDefaultLen:]
	ans.Source = ansDefaultData
	// 解析字符串当前数据
	ansNowData := v[NAME_LEN+INFO_LEN+MAX_STRING_LEN:][MAX_STRING_LEN-ansNowLen:]
	ans.Now = ansNowData
	return ans, nil
}

// 获取变量的值
func Get(data *modules.ParseString) string {
	if data.NowStatus {
		return data.Now
	} else {
		return data.Source
	}
}

// 修改变量值
func ChangeValue(data *modules.ParseString, newValue string) (*modules.ParseString, error) {
	if len([]byte(newValue)) > MAX_STRING_LEN {
		return nil, fmt.Errorf("%v 新修改的变量值超出设计长度", data.Name)
	}
	data.Now = newValue
	data.NowLen = len(data.Now)
	data.NowStatus = true
	data.Data = change(data)
	return data, nil
}

// 将格式化字符串数据结构的值 并转换成字符串
func change(data *modules.ParseString) string {
	ans := data.Data
	// 修改 字符串当前长度信息
	ans = ans[:NAME_LEN+MAX_VALUE_NAME_LEN+len([]byte(strconv.Itoa(MAX_STRING_LEN)))] + parse(strconv.Itoa(data.NowLen), len([]byte(strconv.Itoa(MAX_STRING_LEN)))) + ans[NAME_LEN+MAX_VALUE_NAME_LEN+len([]byte(strconv.Itoa(MAX_STRING_LEN)))+len([]byte(strconv.Itoa(MAX_STRING_LEN))):]
	// 修改 原字符串修改状态
	ans = ans[:NAME_LEN+INFO_LEN-1] + "1" + ans[NAME_LEN+INFO_LEN-1+1:]
	// 修改 字符串当前数据
	ans = ans[:NAME_LEN+INFO_LEN+MAX_STRING_LEN] + parse(data.Now, MAX_STRING_LEN)
	data.Data = ans
	return ans
}

// 将变量对象编码成字符串
func Encode(name, v string) (string, error) {
	if len([]byte(name)) > MAX_STRING_LEN {
		return "", fmt.Errorf("%v 的变量名超出设计长度", name)
	}
	// 装填 字符串特征头部
	ans := MOD_NAME + MOD_STRING_HEAD + MOD_STRING_HEAD_RANDOM
	tempInfoName := parse(name, MAX_VALUE_NAME_LEN)
	// 装填 字符串属性信息的 字符串名称
	ans = ans + tempInfoName
	tempData := v
	if len([]byte(tempData)) > len([]byte(strconv.Itoa(MAX_STRING_LEN))) {
		return "", fmt.Errorf("%v 的变量值超出设计长度", name)
	}
	tempDataLen := parse(strconv.Itoa(len([]byte(tempData))), len([]byte(strconv.Itoa(MAX_STRING_LEN))))
	// 装填 字符串属性信息的 字符串初始值长度
	ans = ans + tempDataLen
	// 装填 字符串属性信息的 字符串真实值长度
	ans = ans + parse("0", len([]byte(strconv.Itoa(MAX_STRING_LEN))))
	// 装填 字符串属性信息的 当前变量为初始化还是修改
	ans = ans + "0"
	// 装填 字符串内容组成
	tempData = parse(tempData, MAX_STRING_LEN)
	ans = ans + tempData + parse("", MAX_STRING_LEN)

	return ans, nil
}
