# StaticValue

#### 介绍
可以定义一种可变静态常量 使用该库定义的常量 可以根据程序运行时动态修改二进制内部的数据 达到运行时永久修改程序内部本身常量的目的

#### 使用说明

1.  写好常量定义配置文件
2.  使用库本身自带的cli来根据定义常量配置文件生成源码
3.  将源码加入项目中使用
#### 注意

1.  只支持字符串类型变量
2.  定义的静态常量初始化时不能拥有 反引号 ` 字符
3.  静态常量最大可用长度为 32768
4.  目前还不支持windows端
5.  cli程序默认识别解析当前目录下的StaticValues.toml文件
6.  线程不安全

#### 常量定义配置文件示例

```toml
[[values]]
name = "Value1"
value = "66"

[[values]]
name = "Value2"
value = "555"

[[values]]
name = "Value3"
value = "555"
```

#### 使用示例

```go
package main

import (
    "fmt"

    tmpl "StaticValueDemo/s"
)

func main() {
    s := tmpl.S
    fmt.Println("第一次获取值")
    fmt.Println(s.Get(tmpl.Name("Value2")))
    fmt.Println(s.Get(tmpl.Name("Value1")))
    fmt.Println(s.Get(tmpl.Name("Value3")))
    fmt.Println(s.Change(tmpl.Name("Value2"), "我是新的值"))
    fmt.Println(s.Get(tmpl.Name("Value2")))
}
// 首次编译运行结果:
//
// 第一次获取值
// 555 <nil>
// 66 <nil>
// 555 <nil>
// <nil>
// 我是新的值 <nil>
//
// 第二次运行结果:
//
// 第一次获取值
// 我是新的值 <nil>
// 66 <nil>
// 555 <nil>
// <nil>
// 我是新的值 <nil>
```