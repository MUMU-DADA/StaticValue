package sv

import (
	"bytes"

	parsevalue "gitee.com/MUMU-DADA/StaticValue/ParseValue"
	"gitee.com/MUMU-DADA/StaticValue/modules"
)

// const (
// 	HEAD = "HEAD_STATICVALUE_" // 变量识别用前缀
// )

// 变量类
type SV struct {
	Name   string // 变量名
	Head   int    // 头部位置
	MaxLen int    // 初始化后最大长度
	NowLen int    // 当前长度
	Value  string // 动态真实值

	ParseValue *modules.ParseString // 解析值
}

// 修改内存中的当前值
func (s *SV) changeBin(bin []byte) []byte {
	ans := append(bin[:s.Head], []byte(s.ParseValue.Data)...)
	ans = append(ans, bin[s.Head+parsevalue.ALL_LEN:]...)
	return ans
}

// 修改变量的新值
func Change(s *SV, bin []byte, newValue string) ([]byte, error) {
	// 修改属性值
	tempParseString, err := parsevalue.ChangeValue(s.ParseValue, newValue)
	if err != nil {
		return nil, err
	}
	s.ParseValue = tempParseString
	s.NowLen = s.ParseValue.NowLen
	s.Value = s.ParseValue.Now
	// 修改二进制
	return s.changeBin(bin), nil
}

// 将所有注册静态变量加载进动态数据中
func LoadAllValue(bin []byte) ([]*SV, error) {
	ans := make([]*SV, 0)
	// 寻找静态变量个数
	count := bytes.Count(bin, []byte(parsevalue.MOD_NAME+parsevalue.MOD_STRING_HEAD+parsevalue.MOD_STRING_HEAD_RANDOM))
	// 创建一个深层缓存拷贝
	var tempBin []byte = make([]byte, len(bin))
	copy(tempBin, bin)
	// 循环获取每个静态变量信息
	for i := 0; i < count; i++ {
		// 寻找第一个出现的变量的头位置
		tempHead := bytes.Index(tempBin, []byte(parsevalue.MOD_NAME+parsevalue.MOD_STRING_HEAD+parsevalue.MOD_STRING_HEAD_RANDOM))
		var tempSourceByte []byte = make([]byte, len(tempBin[tempHead:tempHead+parsevalue.ALL_LEN]))
		copy(tempSourceByte, tempBin[tempHead:tempHead+parsevalue.ALL_LEN])
		if string(tempSourceByte[parsevalue.NAME_LEN:parsevalue.NAME_LEN+len(parsevalue.RANDOM_NAME_HEAD)]) != parsevalue.RANDOM_NAME_HEAD {
			// 遇见连续定义的 标志位常量 跳过
			// 截取掉使用的部分 剩下的部分进行接下来的再次寻找
			tempBin = tempBin[tempHead+parsevalue.NAME_LEN+parsevalue.INFO_LEN:]
			continue
		}
		tempParse, err := parsevalue.Decode(string(tempSourceByte))
		if err != nil {
			return nil, err
		}
		tempSV := &SV{
			Name:   tempParse.Name,
			Head:   bytes.Index(bin, tempSourceByte),
			MaxLen: parsevalue.MAX_STRING_LEN,
			NowLen: tempParse.SourceLen,

			ParseValue: tempParse,
		}
		if tempParse.NowStatus {
			tempSV.Value = tempParse.Now
		} else {
			tempSV.Value = tempParse.Source
		}
		ans = append(ans, tempSV)
		// 截取掉使用的部分 剩下的部分进行接下来的再次寻找
		tempBin = tempBin[tempHead+parsevalue.ALL_LEN:]
	}
	return ans, nil
}
