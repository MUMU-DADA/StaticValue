package FileEngine

import (
	"fmt"
	"os"
	"path/filepath"

	"gitee.com/MUMU-DADA/StaticValue/sv"
)

// 执行文件本身
type FileEngine struct {
	binPath         string   // 载入程序路径
	binName         string   // 载入程序二进制名称
	binData         []byte   // 载入的程序动态文件
	staticValueList []*sv.SV // 所存变量列表
}

// 获取静态变量的值
func (f *FileEngine) Get(name string) *sv.SV {
	for _, v := range f.staticValueList {
		if v.Name == name {
			return v
		}
	}
	return nil
}

// 修改静态变量的值
func (f *FileEngine) Change(name string, newValue string) error {
	for i, v := range f.staticValueList {
		if v.Name == name {
			// 获取原数据的拷贝
			tempSVCopy := *v
			newBin, err := sv.Change(v, f.binData, newValue)
			if err != nil {
				// 恢复
				f.staticValueList[i] = &tempSVCopy
				return err
			}
			if err := f.changeRealBin(newBin); err != nil {
				// 恢复数据拷贝
				f.staticValueList[i] = &tempSVCopy
				return err
			}
			f.binData = newBin
			return nil
		}
	}
	return fmt.Errorf("没有找到该变量")
}

// 将新数据写入文件
func (f *FileEngine) changeRealBin(newBin []byte) error {
	if err := os.Remove(f.binPath + f.binName); err != nil {
		return err
	}
	if err := os.WriteFile(f.binPath+f.binName, f.binData, os.ModePerm); err != nil {
		return err
	}
	return nil
}

// 初始化加载全部程序现有变量
func NewFileEngine() (*FileEngine, error) {
	ans := &FileEngine{}
	// 装填文件信息
	path, err := os.Executable()
	if err != nil {
		return nil, err
	}
	binPath, binName := filepath.Split(path)
	ans.binPath = binPath
	ans.binName = binName
	binData, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	ans.binData = binData

	// 寻找文件内部的变量文件
	ans.staticValueList, err = sv.LoadAllValue(ans.binData)
	if err != nil {
		return nil, err
	}

	return ans, nil
}
