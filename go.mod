module gitee.com/MUMU-DADA/StaticValue

go 1.18

require (
	github.com/go-basic/uuid v1.0.0
	github.com/pelletier/go-toml v1.9.4
)
