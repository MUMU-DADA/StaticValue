package StaticValue

import (
	"fmt"
	"runtime"

	"gitee.com/MUMU-DADA/StaticValue/FileEngine"
)

type StaticValue struct {
	data       *FileEngine.FileEngine // 载入的程序动态文件
	loginValue []map[string]string    //注册的静态变量数据
}

// 新建一个实例
func NewStaticValue() (*StaticValue, error) {
	if runtime.GOOS == "windows" {
		return nil, fmt.Errorf("windows are not support")
	}
	return &StaticValue{}, nil
}

// 获取一个变量的值
func (s *StaticValue) Get(name string) (string, error) {
	ans := s.data.Get(name)
	if ans == nil {
		return "", fmt.Errorf("没有找到该变量")
	}
	return ans.Value, nil
}

// 修改一个变量的值
func (s *StaticValue) Change(name string, newValue string) error {
	if err := s.data.Change(name, newValue); err != nil {
		return err
	}
	return nil
}

// 注册一个变量 (主要是用于使用常量 使编译器对常量进行编译保存)
func (s *StaticValue) SignStaticValue(data string, name string) {
	s.loginValue = append(s.loginValue, map[string]string{name: data})
}

// 加载静态变量数据
func (s *StaticValue) Init() error {
	data, err := FileEngine.NewFileEngine()
	if err != nil {
		return err
	}
	s.data = data
	return nil
}
